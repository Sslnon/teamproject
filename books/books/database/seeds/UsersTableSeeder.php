<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('flights')->insert([
            'account' => "T21309123",
            'name' => "張志傑",
            'position' => "系辦助理",
            'department' => "資管系",
            'login' => "officer",
            'date' => new Carbon('next year')
        ]);
    }
}
