<!DOCTYPE html>
<html lang="en">
<head>
    <title>校務研究辦公室 建立系辦人員</title>
    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link
        rel="icon"
        href="https://www.nuu.edu.tw/var/file/0/1000/msys_1000_9991812_60080.jpg"
        type="image/x-icon"
    />
    <!-- Bootstrap CSS -->
    <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous"
    />
    <link
        href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap"
        rel="stylesheet"
    />
    <style>
        * {
            font-family: "Noto Sans TC", sans-serif;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <a class="navbar-brand" href="/IR">校務研究辦公室 建立系辦人員</a>
</nav>

<!-- Button trigger modal -->

<!-- Modal -->

<section class="mt-5" id="app">
    <div
        class="modal fade"
        id="InputForExcel"
        tabindex="-1"
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
    >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">匯入</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input
                    type="file"
                    id="ExcelAdd"
                    style="display: none"
                    @change="fileChange()"
                    accept=".xlsx, .xls, .csv"
                />
                <input
                    type="file"
                    id="ExcelUpdate"
                    style="display: none"
                    @change="fileChange2()"
                    accept=".xlsx, .xls, .csv"
                />
                <div class="modal-body">
                    <div class="container">
                        <div class="row my-2">
                            <button
                                class="btn btn-success"
                                onclick="$('#ExcelAdd').click()"
                            >
                                新增
                            </button>
                            <span class="m-2"
                            >新增資料在原本資料後方，不會將原資料刪除</span
                            >
                        </div>
                        <div class="row my-2">
                            <button
                                class="btn btn-primary"
                                onclick="$('#ExcelUpdate').click()"
                            >
                                更新
                            </button>
                            <span class="m-2">取代原本資料，不會保留原有資料</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        關閉
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div
        class="modal fade"
        id="TypingInput"
        tabindex="-1"
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
    >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">新增系辦資料</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('flight.store')}}" method="post">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="form-group">
                                    <label for="">帳號</label>
                                    <input
                                        type="text"
                                        name="account"
                                        id=""
                                        class="form-control"
                                        placeholder=""
                                        aria-describedby="helpId"
                                        v-model="type.account"
                                    />
                                    <small id="helpId" class="text-muted">輸入學校帳號</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="">姓名</label>
                                    <input
                                        type="text"
                                        name="name"
                                        id=""
                                        class="form-control"
                                        placeholder=""
                                        aria-describedby="helpId"
                                        v-model="type.name"
                                    />
                                    <small id="helpId" class="text-muted">輸入名稱</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 px-0">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01"
                                            >部門</label
                                            >
                                        </div>
                                        <select
                                            class="custom-select"
                                            id="inputGroupSelect01"
                                            v-model="type.department"
                                            name="department"
                                        >
                                            <template v-for="item in departments">
                                                <option :value="item.name">@{{ item.name }}</option>
                                            </template>



                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 p-0 mt-0">
                                    <small id="helpId" class="text-muted">請選擇科系</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="">有效日期</label>
                                    <input
                                        type="date"
                                        name="date"
                                        id=""
                                        class="form-control"
                                        placeholder=""
                                        aria-describedby="helpId"
                                        v-model="type.date"
                                    />
                                    <small id="helpId" class="text-muted">選擇有效日期</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button
                                type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal"
                            >
                                關閉
                            </button>
                            <button type="submit" class="btn btn-primary">儲存</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Modal -->
    <div
        class="modal fade"
        id="changeinput"
        tabindex="-1"
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
    >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">修改系辦資料</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('flight.update',0)}}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="id" v-model="change.id">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="form-group">
                                    <label for="">帳號</label>
                                    <input
                                        type="text"
                                        name="account"
                                        id=""
                                        class="form-control"
                                        placeholder=""
                                        aria-describedby="helpId"
                                        v-model="change.account"
                                    />
                                    <small id="helpId" class="text-muted">輸入學校帳號</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="">姓名</label>
                                    <input
                                        type="text"
                                        name="name"
                                        id=""
                                        class="form-control"
                                        placeholder=""
                                        aria-describedby="helpId"
                                        v-model="change.name"
                                    />
                                    <small id="helpId" class="text-muted">輸入名稱</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 px-0">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01"
                                            >部門</label
                                            >
                                        </div>
                                        <select
                                            class="custom-select"
                                            id="inputGroupSelect01"
                                            v-model="change.department"
                                            name="department"
                                        >
                                            <template v-for="item in departments">
                                                <option :value="item.name">@{{ item.name }}</option>
                                            </template>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 p-0 mt-0">
                                    <small id="helpId" class="text-muted">請選擇科系</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="">有效日期</label>
                                    <input
                                        type="date"
                                        name="date"
                                        id=""
                                        class="form-control"
                                        placeholder=""
                                        aria-describedby="helpId"
                                        v-model="change.date"
                                    />
                                    <small id="helpId" class="text-muted">選擇有效日期</small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button
                                type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal"
                                @click="CloseInput"
                            >
                                關閉
                            </button>
                            <button type="submit" class="btn btn-primary">儲存</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container" v-if="loading">
        <div class="row">
            <div class="col-12">
                <button
                    type="button"
                    name=""
                    id=""
                    class="btn btn-primary mx-2"
                    data-toggle="modal"
                    data-target="#InputForExcel"
                >
                    匯入系辦資料
                </button>
                <button
                    type="button"
                    name=""
                    id=""
                    class="btn btn-success mx-2"
                    data-target="#TypingInput"
                    data-toggle="modal"
                >
                    新增系辦資料
                </button>
            </div>
            <table class="table mt-2">
                <thead>
                <tr>
                    <th>編號</th>
                    <th>帳號</th>
                    <th>姓名</th>
                    <th>部門</th>
                    <th>有效日期</th>
                    <th>修改</th>
                    <th>刪除</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item,index) in all" >
                    <td scope="row">@{{ index }}</td>
                    <td>@{{ item.account }}</td>
                    <td>@{{ item.name }}</td>
                    <td>@{{ item.department }}</td>
                    <td>@{{ item.date }}</td>
                    <td>@{{ item.date }}</td>
                    <td>
                        <button
                            type="button"
                            name=""
                            id=""
                            class="btn btn-info"
                            @click="changedata(index)"
                        >
                            修改
                        </button>
                    </td>
                    <td>
                        <form action="{{route('flight.destroy',0)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            @csrf
                            <input type="hidden" name="id" v-model="item.id" >
                            <button   type="submit" name="" id="" class="btn btn-danger">
                                刪除
                            </button>
                        </form>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div v-else class="container"  ><h1>loading</h1></div>
</section>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script
    src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"
></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"
></script>
<script
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"
></script>

<script>
    new Vue({
        el: "#app",
        data() {
            return {
                loading:false,
                type: {
                    name: "",
                    account: "",
                    department: "",
                    date: ""
                },
                change: {
                    id: 0,
                    name: "",
                    account: "",
                    department: "",
                    date: ""
                },
                all: [],
                departments:[]
            };
        },
        methods: {
            changedata(index) {
                this.change.id = this.all[index].id;
                this.change.name = this.all[index].name;
                this.change.account = this.all[index].account;
                this.change.department = this.all[index].department;
                this.change.date = this.all[index].date;
                $("#changeinput").modal("show");
            },
            CloseInput() {
                this.type.Name = "";
                this.type.Account = "";
                this.type.Department = "";
                $("#TypingInput").modal("hide");
            },
            fileChange(e) {
                let formData = new FormData();
                console.log("add" + event.target.files[0]);
                formData.append("file", event.target.files[0]);
                axios.post("API", formData);
            },
            fileChange2(e) {
                let formData = new FormData();
                console.log("update" + event.target.files[0]);
                formData.append("file", event.target.files[0]);
                axios.post("API", formData);
            }
        },
        created() {
            this.loading=false;
            axios.get("http://127.0.0.1:8000/flight/show").then(res => {
                this.all = res.data;
            });
            axios.get("http://127.0.0.1:8000/departments/show").then(res => {
                this.departments = res.data;
                this.loading=true;
            });
        }
    });
</script>
</body>
</html>
