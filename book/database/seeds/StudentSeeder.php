<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $Student = [
            ['Snum'=>'S043546','name'=>'S043546',"part"=>"日校",'department'=>"im","date"=>"2019-08-12"],
            ['Snum'=>'S023123','name'=>'S023123',"part"=>"日校",'department'=>"im","date"=>"2019-08-12"],
            ['Snum'=>'S024246','name'=>'S024246',"part"=>"日校",'department'=>"im","date"=>"2019-08-12"],
            ['Snum'=>'S033521','name'=>'S033521',"part"=>"日校",'department'=>"im","date"=>"2019-08-12"],
            ['Snum'=>'S044352','name'=>'S044352',"part"=>"日校",'department'=>"im","date"=>"2019-08-12"],
            ['Snum'=>'S023123','name'=>'S023123',"part"=>"日校",'department'=>"im","date"=>"2019-08-12"]
        ];
        foreach ($Student as $Students) {
            DB::table('students')->insert($Students);
        }
    }
}
