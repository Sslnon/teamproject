<?php

use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $Teacher = [
            ['Tnum'=>'T343546','name'=>'T343546','department'=>"im","Account"=>"T123456","date"=>"2019-08-12"],
            ['Tnum'=>'T123123','name'=>'T123123','department'=>"im","Account"=>"T123457","date"=>"2019-08-12"],
            ['Tnum'=>'T324246','name'=>'T324246','department'=>"im","Account"=>"T123458","date"=>"2019-08-12"],
            ['Tnum'=>'T233521','name'=>'T233521','department'=>"im","Account"=>"T123459","date"=>"2019-08-12"],
            ['Tnum'=>'T344352','name'=>'T344352','department'=>"im","Account"=>"T123451","date"=>"2019-08-12"],
            ['Tnum'=>'T123123','name'=>'T123123','department'=>"im","Account"=>"T123453","date"=>"2019-08-12"]
        ];
        foreach ($Teacher as $Teachers) {
            DB::table('teachers')->insert($Teachers);
        }
    }
}
