<?php

use Illuminate\Database\Seeder;

class matchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $departments = [
            ['department' => 'lc', ],
            ['department' => 'tic', ],
            ['department' => 'doct', ],
            ['department' => 'ccdm', ],
            ['department' => 'bm', ],
            ['department' => 'id', ],
            ['department' => 'finance', ],
            ['department' => 'arch', ],
            ['department' => 'im', ],
            ['department' => 'indigenous', ],
            ['department' => 'che', ],
            ['department' => 'civil', ],
            ['department' => 'she', ],
            ['department' => 'mse', ],
            ['department' => 'energy', ],
            ['department' => 'csie', ],
            ['department' => 'deeweb', ],
            ['department' => 'mech', ],
            ['department' => 'eo', ],
            ['department' => 'ee', ],
        ];
        foreach ($departments as $department) {
            DB::table('match_nums')->insert($department);
        }
    }

}
