@extends('Department.header')
@section('content')
<section class="mt-5" id="app">
    <div class="container">
        <div class="row">
            <form action="{{route('matchnum.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="">請輸入學生需要幾位老師評分</label>
                    <input type="text" name="id" id="" class="form-control" placeholder="" aria-describedby="helpId">
                    <small id="helpId" class="text-muted">第一次輸入後按下確定 資料庫產生資料 <span style="color:#ff0000;">若想更改人數 資料庫不保留以選擇人員</span></small>
                </div>
                <button type="submit" name="" id="" class="btn btn-primary">送出</button>
            </form>
            <form action="{{ route('matchnum.store', 0) }}" method="post">

                @csrf
                <input type="hidden" name="id" value="4" />

                <button type="submit" name="" id="" class="btn btn-danger">
                    POST
                </button>
            </form>
        </div>
    </div>

    <div class="container mt-3">
        <div class="row">
            <div class="col-12" v-if="!first">
                <div class="card text-center">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link" :class="{active : nowselected }" @click="nowselected=1"
                                    href="#">未選擇</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" :class="{active : !nowselected }" @click="nowselected=0"
                                    href="#">已選擇</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body" >
                        <div class="row">
                            <div class="col-2 d-flex flex-column">
                                <template v-for="(item, index) in theacher">
                                    <button type="button" name="" id="" class="btn p-2 m-2 "
                                        :class="{ btnPrimary: index == nowteacher }" @click="changeTeacher(index)">
                                        @{{item}}</button>
                                </template>
                            </div>
                            <div class="col " v-if="nowselected">
                                <form action="">
                                    <div class="row h-100">
                                        <div class="col-12  d-flex flex-wrap">
                                            <template v-for="(item, index) in theacher">
                                                <template v-for="(item1, index2) in student[index]">
                                                    <div class="form-check"
                                                        :class="{ disable: index != nowteacher }">
                                                        <input type="text"  name="" id="" :value="item">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" class="form-check-input"
                                                                name="student" id="" value="checkedValue">
                                                            <i class="fa fa-user fa-4x " aria-hidden="true"
                                                                :style=""></i>
                                                            @{{item1}}
                                                        </label>
                                                    </div>
                                                </template>
                                            </template>
                                        </div>
                                    </div>
                                    <div class="row mt-5">
                                        <div class="col-12">
                                            <div>
                                                <button type="button" name="" id=""
                                                    class="btn btn-primary">送出</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col d-flex flex-wrap" v-else>
                                <template v-for="(item, index) in theacher">
                                    <template v-for="(item1, index2) in selected[index]">
                                        <div class="form-check" :class="{ disable: index != nowteacher }">
                                            <label class="form-check-label">
                                                <i class="fa fa-user fa-4x " aria-hidden="true" :style=""></i>
                                                @{{item1}}
                                            </label>
                                        </div>
                                    </template>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    @parent
    <script>
        new Vue({
            el: "#app",
            data() {
                return {
                    theacher: ['1', '2', '3', '4'],
                    student: [],
                    selected: [],
                    nowteacher: 0,
                    nowselected: 1,
                    first:true
                }
            },
            methods: {
                changeTeacher(index) {
                    this.nowteacher = index;
                }
            },
            created() {
                // for (let i = 0; i < this.theacher.length; i++) {
                //     this.student[i] = new Array();
                //     for (let j = 0; j < 20 + i; j++) {
                //         this.student[i][j] = j + 1;
                //     }
                // }
                // for (let i = 0; i < this.theacher.length; i++) {
                //     this.selected[i] = new Array();
                //     for (let j = 0; j < 3 + i; j++) {
                //         this.selected[i][j] = j + 1;
                //     }
                // }
                // console.log(this.student);
                axios.get("http://book.test/matchnum/show").then(res=>{
                    if (res.data.length!=0){
                        first=true;
                    } else {
                        first=false;
                    }
                })
            },
        })
    </script>
@endsection
