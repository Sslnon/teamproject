@extends('Department.header') @section('content')
<section class="mt-5" id="app">
  <div
    class="modal fade"
    id="InputForExcel"
    tabindex="-1"
    role="dialog"
    aria-labelledby="modelTitleId"
    aria-hidden="true"
  >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">匯入</h5>
          <button
            type="button"
            class="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <input
          type="file"
          id="ExcelAdd"
          style="display: none"
          @change="fileChange()"
          accept=".xlsx, .xls, .csv"
        />
        <input
          type="file"
          id="ExcelUpdate"
          style="display: none"
          @change="fileChange2()"
          accept=".xlsx, .xls, .csv"
        />
        <div class="modal-body">
          <div class="container">
            <div class="row my-2">
              <button class="btn btn-success" onclick="$('#ExcelAdd').click()">
                新增
              </button>
              <span class="m-2">新增資料在原本資料後方，不會將原資料刪除</span>
            </div>
            <div class="row my-2">
              <button
                class="btn btn-primary"
                onclick="$('#ExcelUpdate').click()"
              >
                更新
              </button>
              <span class="m-2">取代原本資料，不會保留原有資料</span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            關閉
          </button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div
    class="modal fade"
    id="changeinput"
    tabindex="-1"
    role="dialog"
    aria-labelledby="modelTitleId"
    aria-hidden="true"
  >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">修改學生資料</h5>
          <button
            type="button"
            class="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('studentdata.update', 0) }}" method="post">
            <input type="hidden" name="_method" value="PUT" />
            <input type="hidden" name="id" v-model="change.id" />
            @csrf
            <div class="container">
              <div class="row">
                <div class="form-group">
                  <label for="">准考證號碼</label>
                  <input
                    type="text"
                    name="account"
                    id=""
                    class="form-control"
                    placeholder=""
                    aria-describedby="helpId"
                    v-model="change.account"
                    required
                  />
                  <small id="helpId" class="text-muted">輸入准考證號碼</small>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">姓名</label>
                  <input
                    type="text"
                    name="name"
                    id=""
                    class="form-control"
                    placeholder=""
                    aria-describedby="helpId"
                    v-model="change.name"
                  />
                  <small id="helpId" class="text-muted">輸入名稱</small>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">有效日期</label>
                  <input
                    type="date"
                    name="date"
                    id=""
                    class="form-control"
                    placeholder=""
                    aria-describedby="helpId"
                    v-model="change.date"
                    required
                  />
                  <small id="helpId" class="text-muted">選擇有效日期</small>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
                @click="CloseInput"
              >
                關閉
              </button>
              <button type="submit" class="btn btn-primary">儲存</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container" v-if="loading">
    <div class="row">
      <div class="col-12">
        <button
          type="button"
          name=""
          id=""
          class="btn btn-primary mx-2"
          data-toggle="modal"
          data-target="#InputForExcel"
        >
          匯入學生資料
        </button>
      </div>
      <table class="table mt-2">
        <thead>
          <tr>
            <th>編號</th>
            <th>准考證號碼</th>
            <th>姓名</th>
            <th>部門</th>
            <th>有效日期</th>
            <th>修改</th>
            <th>刪除</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(item,index) in all">
            <td scope="row">@{{ index + 1 }}</td>
            <td>@{{ item.account }}</td>
            <td>@{{ item.name }}</td>
            <td>@{{ item.department }}</td>
            <td>@{{ item.date }}</td>
            <td>
              <button
                type="button"
                name=""
                id=""
                class="btn btn-info"
                @click="changedata(index)"
              >
                修改
              </button>
            </td>
            <td>
              <form action="{{ route('studentdata.destroy', 0) }}" method="post">
                <input type="hidden" name="_method" value="DELETE" />
                @csrf
                <input type="hidden" name="id" v-model="item.id" />
                <button type="submit" name="" id="" class="btn btn-danger">
                  刪除
                </button>
              </form>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div v-else class="container"><h1>loading</h1></div>
</section>

@endsection @section('script') @parent
<script>
  new Vue({
    el: "#app",
    data() {
      return {
        loading: false,
        type: {
          name: "",
          account: "",
          department: "",
          date: ""
        },
        change: {
          id: 0,
          name: "",
          account: "",
          department: "",
          date: ""
        },
        all: [],
        departments: []
      };
    },
    methods: {
      changedata(index) {
        this.change.id = this.all[index].id;
        this.change.name = this.all[index].name;
        this.change.account = this.all[index].account;
        this.change.department = this.all[index].department;
        this.change.date = this.all[index].date;
        $("#changeinput").modal("show");
      },
      CloseInput() {
        console.log("close");
        this.type.name = "";
        this.type.account = "";
        this.type.department = "";
        this.type.date = "";
        $("#TypingInput").modal("hide");
      }
    },
    created() {
      this.loading = false;
      axios.get("http://127.0.0.1:8000/studentdata/show").then(res => {
        this.all = res.data;
        this.loading = true;
      });
    }
  });
</script>
@endsection
