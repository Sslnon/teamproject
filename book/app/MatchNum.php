<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchNum extends Model
{
    //
    protected $table="match_nums";
    protected $fillable = ['number'];
}
