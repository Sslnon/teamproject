<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark_data extends Model
{
    //
    protected $table = "mark_datas";
    protected $fillable = [
        'Bitem', 'Mitem', 'Sitem','title','HighScore','lowScore','date'
    ];
}
