<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    //
    protected $table = "students";
    protected $fillable = [
        'Snum', 'name','department','part','date'
    ];

}
